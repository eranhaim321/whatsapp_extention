import React, { useState, useEffect } from "react";
import { Button, LinearProgress, TextField } from "@material-ui/core";
import qrcode from "qrcode";
export default function App() {
  const [QR, setQR] = useState("");
  const [chats, setChats] = useState([]);
  const [ID, setID] = useState(0);
  const [pic, setPic] = useState("");
  const [isWaitigForQR, setIsWaitingForQR] = useState(false);

  useEffect(() => {
    setIsWaitingForQR(false);
  }, [QR]);
  return (
    <div
      style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <div>id: {ID}</div>
      <Button
        variant="contained"
        onClick={() => {
          setIsWaitingForQR(true);
          fetch("http://DESKTOP-8J8L6K0:5555/getQR")
            .then((res) => res.json())
            .then((res) => {
              console.log(res.qr);
              setID(res._id);
              qrcode.toDataURL(res.qr).then((data) => {
                setQR(data);
                setIsWaitingForQR(false);
              });
            });
        }}
      >
        קבל קוד QR
      </Button>
      <div style={{ marginTop: 10, width: 250 }}>
        {isWaitigForQR ? <LinearProgress /> : <img src={QR} />}
      </div>
      <Button
        variant="contained"
        color="primary"
        onClick={() =>
          fetch(`http://DESKTOP-8J8L6K0:5555/getProfilePic/${ID}`).then((res) =>
            res.json().then((res) => setPic(res.pic))
          )
        }
      >
        קבל תמונת פרופיל
      </Button>
      <img src={pic} style={{ margin: 10, width: 100 }} />
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          fetch(`http://DESKTOP-8J8L6K0:5555/getChats/${ID}/10`)
            .then((res) => res.json())
            .then((res) => {
              setChats(res.chats);
              console.log(res.chats);
            });
        }}
      >
        קבל צ'אטים
      </Button>
      <div>{JSON.stringify({ chats })}</div>
    </div>
  );
}
