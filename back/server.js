const express = require("express");
const nedb = require("nedb");
const cors = require("cors");
const app = express();
const { Client, LocalAuth } = require("whatsapp-web.js");

const clients = new nedb("clients.db");
clients.loadDatabase();

const sessionClients = {};

app.use(express.json());
app.use(cors({ origin: "*" }));

app.get("/getQR", (req, res) => {
  console.log("someone asked for QR");
  let _id = "" + new Date().getTime();
  let client = new Client({
    authStrategy: new LocalAuth({ clientId: _id }),
  });
  client.on("qr", (qr) => {
    res.json({ qr: qr, _id });
  });
  client.on("ready", () => {
    console.log("ready!", client.info);
    clients.insert({ _id, ...client.info });
    sessionClients[_id] = client;
  });
  client.initialize();
});

app.get("/getChats/:_id/:amount", (req, res) => {
  const { _id, amount } = req.params;
  if (sessionClients[_id]) {
    sessionClients[_id].getChats().then((chats) => {
      res.json({ chats: chats.slice(0, parseInt(amount)) });
    });
  } else res.json({ chats: [] });
});

app.get("/getMsgs/:_id/:chatID", (req, res) => {
  const { _id, chatID } = req.params;
  if (sessionClients[_id]) {
    sessionClients[_id].getChatById(chatID).then((chats) => {
      chatsWithMessages.forEach((chat) => {
        chat.fetchMessages({ limit: 10 }).then((msgs) => {
          chat = { ...chat, msgs };
        });
      });
      res.json({ msgs: chats.slice(0, parseInt(amount)) });
    });
  } else res.json({ msgs: [] });
});

// app.get("/getProfilePic/:index", (req, res) => {
//   const { index } = req.params;
//   new Client().get
//   clients[index].getProfilePicUrl().then((pic) => res.json({ pic: pic }));
// });

app.post("/setDelayedMsg", (req, res) => {
  res.end();
});

app.get("/getUserDelayedMsgs/:num", (req, res) => {
  res.end();
});

app.listen(5555, () => console.log("now listening to port 5555"));
